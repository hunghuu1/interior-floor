function scrollToElement(idclick,idbutton){
    if(typeof idclick === 'string' && typeof idbutton === 'string'){

        let click = document.querySelector(idclick);
        let button = document.querySelector(idbutton);

        if(click && button ){
            click.addEventListener('click',function(){
                button.scrollIntoView({
                    behavior: "smooth", 
                    block: "start", 
                    inline: "nearest",
                });
            })
        }
    }
}

scrollToElement('.banner__content','#footer');

function scrollToMenu(idlist,idmenu){
    if( typeof idlist === 'string' && typeof idmenu === 'string'){

        let list = document.querySelector(idlist);
        let menu = document.querySelector(idmenu);

        if(list && menu){

            list.addEventListener('click',function(){
                menu.classList.toggle('active')
            })
        }
    }
}

scrollToMenu('#menu','#item');

